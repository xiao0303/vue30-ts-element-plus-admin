/**
 * @author jerryxiao
 * @time  2022-07-22
 * @title 网络请求api列表
 * @desc 后期分离优化
 *
 */
 import { getData,postData } from '@/utils/http'


 export class account {

    /**
     * 登录
     * */
    static  login(params:any) {
        return postData('/api/v1/oauth/token',params,false);
    }
    /**
     * 菜单
     * */
     static  currentMenu(params:any) {
        return postData('/api/v1/menu/current-tree',params,false);
    }
    /**
     * 方法列表
     * */
     static  funList(params:any) {
        return postData('/api/v1/menu/funList',params,false);
    }

    
    /**
     * 用户列表
     * */
     static  userList(params:any) {
        return postData('/api/v1/user/userList',params,false);
    }
    /**
     * 聊天列表
     * */
     static  chatList(params:any) {
        return postData('/api/v1/user/chatList',params,false);
    }
    /**
     * 发送聊天信息
     * */
    static  getSend(params:any) {
        return postData('/api/v1/user/getSend',params,false);
    }
    /**
     * 旗帜列表
     * */
     static  flagPage(params:any) {
        return postData('/api/v1/flag/flagPage',params,false);
    }
    /**
     * 产品列表
     * */
     static  productPage(params:any) {
        return postData('/api/v1/product/productPage',params,false);
    }

 }

