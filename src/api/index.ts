/**
 * @author jerryxiao
 * @time  2022-07-22
 * @title 网络请求api列表
 * @desc 后期分离优化
 *
 */
 import {account} from './modules/account';
 import { weChat } from './modules/weChat';

 export default {
    account,
    weChat
 }