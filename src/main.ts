import { createApp } from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";

import ElementPlus from 'element-plus';
import 'element-plus/dist/index.css';
// 语言包
import i18n from '@/locales/i18n';
import zhCn from 'element-plus/es/locale/lang/zh-cn';
import enCn from 'element-plus/es/locale/lang/en'
import 'element-plus/theme-chalk/base.css';

import getAddRoute from "@/common/newRoute";
import vconsole from 'vconsole'
if(process.env.NODE_ENV=='development'){
    // new vconsole()
}

// 深色模式
import 'element-plus/theme-chalk/dark/css-vars.css';
// 修改部分深色模式
import './styles/dark.scss';
// 引用iconfont
import './iconfont/iconfont.css';
//公共css
import './styles/index.scss';

const app = createApp(App);
import api from './api/index'
app.config.globalProperties.$api = api;

require('./mock')

// 刷新的时候更新路由
getAddRoute();
app.use(store);
app.use(router);
app.use(i18n);
app.use(ElementPlus,{
    size: 'default',
    locale: zhCn,
    zIndex: 3000
});
app.mount("#app");