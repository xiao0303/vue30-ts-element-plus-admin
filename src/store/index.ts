import { createStore } from "vuex";
import VuexPersistence from "vuex-persist";
import app from "./modules/app";
import setting from "./modules/setting";
const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})
export default createStore({
  strict: false,
  state: {
   
  },
  mutations: {

  },
  actions: {

  },
  modules: {
    app,
    setting
  },
  plugins: [vuexLocal.plugin]
});
