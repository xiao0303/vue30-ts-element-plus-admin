import {familyTree} from "@/utils/index";
import getAddRoute from "@/common/newRoute"
const app = {
    state: () => ({
        isMobile:false,
        token:'',
        isCollapse: false,
        isChat: false,
        menuActive: '',
        nav:[],
        menuList:[],
        breadcrumb:[],
        tableHeight:document.documentElement.clientHeight*0.66
    }),
    mutations: {
        getIsMobile(state:any, newState:any){
            state.isMobile = newState.isMobile;
        },
        getChat(state:any, newState:any){
            state.isChat = newState.isChat;
        },
        getCollapse(state:any, newState:any) {
            state.isCollapse = newState.isCollapse;
        },
        getNav(state:any, newState:any) {
            if(state.nav.length>0){
                let i=-1;
                state.nav.forEach((element:any,index:number) => {
                    if(element.menuId.toString()==newState.nav.menuId){
                        i=index;
                    }
                });
                if(i==-1){
                    state.nav.push(newState.nav);
                }
            }else{
                state.nav.push(newState.nav);
            }
        },
        getMenuActive(state:any, newState:any) {
            state.breadcrumb=familyTree(state.menuList,newState.menuActive);
            state.menuActive = newState.menuActive;
        },
        getTabRemove(state:any, newState:any) {
            state.nav = state.nav.filter((tab:any) => tab.menuId.toString() != newState.menuId);
        },
        getMenuList(state:any, newState:any){
            state.menuList = newState.menuList;
        },
        getToken(state:any, newState:any){
            state.token = newState.token;
        },
        getOutin(state:any, newState:any){
            state.token='';
            state.isCollapse=false;
            state.menuActive='';
            state.nav=[];
            state.menuList=[];
            state.breadcrumb=[];
        },
        getAddNewRoute(state:any, newState:any){
            getAddRoute();
        }
    },
    actions: {
        setIsMobile(context:any, newState:any){
            context.commit('getIsMobile', newState)
        },
        setChat(context:any, newState:any){
            context.commit('getChat', newState)
        },
        setCollapse(context:any, newState:any) {
            context.commit('getCollapse', newState)
        },
        setNav(context:any, newState:any) {
            context.commit('getNav', newState)
        },
        setMenuActive(context:any, newState:any) {
            context.commit('getMenuActive', newState)
        },
        setTabRemove(context:any, newState:any) {
            context.commit('getTabRemove', newState)
        },
        setMenuList(context:any, newState:any) {
            context.commit('getMenuList', newState)
        },
        setToken(context:any, newState:any){
            context.commit('getToken', newState)
        },
        setOutin(context:any, newState:any){
            context.commit('getOutin', newState)
        },
        setAddNewRoute(context:any, newState:any){
            context.commit('getAddNewRoute', newState)
        },
    }
};
export default app;