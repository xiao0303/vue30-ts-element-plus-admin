const setting = {
    state: {
        // 深色模式
        isDark: false,
        showSettings: false,//是否显示设置
        sidebarLogo: true,//是否显示logo
        tagsView: true,//是否显示tag
        tabTheme:"theme1",
        // 主题一
        themeBackground: "#1d1e1f",
        themeColor:"#ffffff",
        themeActive:"#ffd04b",
        headColor:"#ffffff",

        // 主题二
        // themeBackground: "#3f78b2",
        // themeColor:"#ffffff",
        // themeActive:"#ffd04b",
        // headColor:"#ffffff",
        // 底部信息
        copyrightName:'呼啦鸽管理后台',
        copyrightLink:'http://dev-ubs-admin.flag.vip/Login'
    },
    mutations: {
        getIsDark(state:any, newState:any){
            state.isDark = newState.isDark;
        },
        getShowSettings(state:any, newState:any) {
            state.showSettings = newState.show;
        },
        getSetting(state:any, newState:any){
            state.tabTheme=newState.tabTheme;
            state.sidebarLogo=newState.sidebarLogo;
            state.tagsView=newState.tagsView;
            state.themeBackground=newState.themeBackground;
            state.themeColor=newState.themeColor;
            state.themeActive=newState.themeActive;
            state.headColor=newState.headColor;
        },
        getOutInSetting(state:any, newState:any){
            state.tabTheme='theme1';
            state.showSettings= false;
            state.sidebarLogo= true;
            state.tagsView= true;
            state.themeBackground= "#1d1e1f";
            state.themeColor= "#ffffff";
            state.themeActive= "#ffd04b";
            state.headColor="#ffffff";
        },
    },
    actions: {
        setIsDark(context:any, newState:any){
            context.commit('getIsDark', newState)
        },
        setShowSettings(context:any, newState:any) {
            context.commit('getShowSettings', newState)
        },
        setSetting(context:any, newState:any){
            context.commit('getSetting', newState)
        },
        setOutInSetting(context:any, newState:any){
            context.commit('getOutInSetting', newState)
        },
    }
};
export default setting;