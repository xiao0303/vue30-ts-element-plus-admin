import  $store  from '@/store/index';
import {menuTree} from "@/utils"
import router from "@/router"
const  getAddRoute=()=>{
    let state=$store.state;
    const NewMenusList=menuTree((state as any).app.menuList);
    if(NewMenusList.length>0){
        let newRoute : Array<any> = [];
        NewMenusList.forEach((item:any)=>{
            newRoute.push({
                name: item.menuId,
                meta: {title: item.name,menu: item.menu},
                path: item.path,
                url:item.url?item.url:'404',
                component:loadView(item.url)
            })
        })
        router.addRoute(
            {
                path: "/",
                component: () => import("../layout/index.vue"),
                children: [
                    ...newRoute
                ]
            }
        );
        console.info("newRoute",newRoute);
        console.info("加载路由",router.getRoutes());
    }
}
const loadView = (view: any) => {
    view = view.replace(/(^\/*)/g, '');
    return () => require.ensure([], (require) => require('@/views/'+view));
}


export default getAddRoute;