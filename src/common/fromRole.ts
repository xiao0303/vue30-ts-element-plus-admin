/**
 * @author jerryxiao
 * @time  2022-08-09
 * @title  表单验证规则
 * @desc
 */

/**
 * 密码验证
 * */
export function fromPwd(message: string) {
	return [{
		required: true,
		message: message,
		trigger: 'blur'
	},
	{
		min: 6,
		max: 20,
		message: '长度在 6 到 20 个字符',
		trigger: 'blur'
	}
	];
}
/**
 * 手机验证
 * */
export function fromPhone(message: string) {
	return [
		{
			required: true,
			message: message,
			trigger: 'blur'
		},
		{
			min: 11,
			max: 11,
			message: '长度最小在 11 个字符',
			trigger: 'blur'
		}
	];
}
/**
 * 长度不为空验证
 * */
export function fromLength(message: string) {
	return [{
		required: true,
		message: message,
		trigger: 'blur'
	},
	{
		min: 1,
		message: '长度在1个字符以上',
		trigger: 'blur'
	}
	];
}
/**
 * 时间选择
 * */
export function fromDate(message: string) {
	return [
		{ type: 'date', required: true, message: message, trigger: 'change' }
	];
}
/**
 * 多选
 * */
export function fromMultiple(message: string) {
	return [
		{ type: 'array', required: true, message: message, trigger: 'change' }
	];
}
/**
 * 单选
 * */
export function fromSingle(message: string) {
	return [
		{ required: true, message: message, trigger: 'change' }
	];
}
/**
 * 数字大于0
 * */
export function fromNumber(message: string) {
	return [
		{ required: true, message: `${message}不能小于0` },
		{ type: 'number', message: `${message}必须为数字值` }
	];
}
/**
 * 必填
 * */
export function fromRequired(message: string) {
	return [
		{ required: true, message: message, trigger: 'blur' }
	];
}
/**
 * 必选
 * */
export function fromSelect(message: string) {
	return [
		{ required: true, message: `${message}` }
	];
}