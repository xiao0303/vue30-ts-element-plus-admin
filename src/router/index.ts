import { createRouter, createWebHistory,createWebHashHistory, RouteRecordRaw } from "vue-router";
import $store from "@/store/index";
import { close, start } from '@/utils/nporgress'



const routes: Array<RouteRecordRaw> = [
  {
    path: "/login",
    name: "login",
    component: () => import("@/views/account/login.vue"),
  },
  {
    path: "/404",
    name: "404",
    component: () => import("@/views/errorPage/404.vue"),
  },
  {
    path: "/:catchAll(.*)",
    redirect: '/404',
  }
];
// createWebHashHistory();
// createWebHistory();
const router = createRouter({
  history: createWebHashHistory(),
  routes,
  scrollBehavior(to,from,savePosition){
    if(savePosition){
      return savePosition;
    }else{
      return {top:0};
    }
  }
});

router.beforeEach((to, from, next) => {
  console.info("to",to);
  // 判断是否存在菜单数组  有:登录中   无：未登录
  start();
  let state=$store.state;
  const NewMenusList=(state as any).app.menuList;
  if (to.matched.length === 0) {
      router.push({
          path: '/404',
          query: {
              T:new Date().getTime()
          }
      });
      next();
  } else {
    if(to.name!='login' && NewMenusList.length==0){
      router.push({
          path: '/login',
          query: {
              T:new Date().getTime()
          }
      });
      next();
    }else{
      // 是否是菜单  是菜单设置选中 和 标签
      if(to.meta.menu==true){
        $store.dispatch('setMenuActive', { menuActive: setString(to.name) })
        $store.dispatch('setNav', { nav: {name:to.meta.title,menuId:to.name,path:to.path} })
      }
      next();
    }
  }
});
const setString = (event:any) =>{
  return event.toString();
}
router.afterEach(() => {
  close()
})
export default router;