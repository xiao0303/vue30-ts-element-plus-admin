/**
 * @author jerry xiao
 * @time  2022-08-08
 * @title  模拟数据
 * @desc
 *
 */
 import Mock from 'mockjs';
 import institution from './data/institution';


 Mock.mock('http://dev.flag.net/api/v1/oauth/token', 'post' ,institution.getToken); // 用户登录
 Mock.mock('http://dev.flag.net/api/v1/menu/current-tree', 'post' ,institution.getMenuList); // 查看菜单列表
 Mock.mock('http://dev.flag.net/api/v1/menu/funList', 'post' ,institution.getFunList); // 查看菜单列表


 Mock.mock('http://dev.flag.net/api/v1/flag/flagPage', 'post' ,institution.flagPage); // 旗帜列表
 Mock.mock('http://dev.flag.net/api/v1/product/productPage', 'post' ,institution.productPage); // 产品列表
 

 Mock.mock('http://dev.flag.net/api/v1/user/userList', 'post' ,institution.getUserList); // 查看用户列表
 Mock.mock('http://dev.flag.net/api/v1/user/chatList', 'post' ,institution.getChatList); // 查看聊天列表表
 Mock.mock('http://dev.flag.net/api/v1/user/getSend', 'post' ,institution.getSend); // 用户发送信息
 
 


 
export default Mock
 