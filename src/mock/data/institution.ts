import Mock, { Random } from 'mockjs';
import menuList from "@/mock/menu";
import {guid} from "@/utils/index";
import { forEach } from 'lodash';
Mock.setup({  //模拟网络延迟
    timeout:"500-1500"
});
const list: any = [];
for (let i = 0; i < 20; i++) {
  list.push({
    id: Random.id(),
    name: Random.cname(),
    code: Random.integer(10000000, 80000000),
    url: Random.url('http'),
    createTime: Random.datetime('yyyy-MM-dd HH:mm'),
    updateTime: Random.datetime('yyyy-MM-dd HH:mm'),
    address: Random.county(true),
    phone: Random.integer(13000000000, 19900000000),
    sex: Random.natural(1, 2),
    age: Random.natural(18, 40),
    status: Random.boolean()
  })
}

// 用户信息
const userList: any = [];
for (let i = 0; i < 20; i++) {
    let name = Random.cname()
    userList.push({
      id: Random.id(),
      name: name,
      badge: Random.natural(0, 10),
      createTime: Random.datetime('yyyy-MM-dd HH:mm'),
      phone: Random.integer(13000000000, 19900000000),
      avatar: Random.image('200x200',Random.color(), '#000000',name),
      desc: Random.csentence()
    })
}

export default {
   /**
   * 旗帜列表
   */
    flagPage: (res: any) => {
        let data = JSON.parse(res.body);
        console.info(data);
        // 旗帜列表
        const flagList: any = [];
        for (let i = 0; i < data.size; i++) {
            flagList.push({
                id: Random.id(),
                name: Random.cname(),
                groupName: Random.csentence(),
                phone: Random.integer(13000000000, 19900000000),
                avatar: Random.image('100x100'),
                remark: Random.csentence(),
            })
        }
        return {
            code: 200,
            total: 6000,
            pageTotal: 300,
            page: data.page,
            data: flagList,
            msg: 'success'
        }
        
    },
    /**
   * 产品列表
   */
     productPage: (res: any) => {
        let data = JSON.parse(res.body);
        console.info(data);
        // 旗帜列表
        const flagList: any = [];
        for (let i = 0; i < data.size; i++) {
            flagList.push({
                id: Random.id(),
                name: Random.cname(),
                groupName: Random.csentence(),
                phone: Random.integer(13000000000, 19900000000),
                img: Random.image('100x100'),
                remark: Random.csentence(),
            })
        }
        return {
            code: 200,
            total: 6000,
            pageTotal: 300,
            page: data.page,
            data: flagList,
            msg: 'success'
        }
        
    },
   /**
   * 登录
   */
    getToken: (res: any) => {
        
        let data = JSON.parse(res.body);
        console.info(data);
        if(data.username=='admin' && data.password=='123456'){
            return {
                code: 200,
                data: {
                    token: '2f0c4caa-c7ec-4da9-a014-7d2897c0352a'
                },
                msg: 'success'
            }
        }else{
            return {
                code: 50010,
                msg: '账号密码错误！'
            }
        }
        
    },
    
   /**
   * 获取列表
   */
    getMenuList: (res: any) => {
        let data = JSON.parse(res.body);
        console.info(data);

        return {
            code: 200,
            data: menuList,
            msg: 'success'
        }
    },
   /**
   * 获取方法列表
   */
    getFunList: (res: any) => {
        let data = JSON.parse(res.body);
        console.info(data);
        const funArr: any = [];
        menuList.forEach((items:any,indexs:any)=>{
            if(items.menuId==5587){
                items.childs.forEach((item:any,index:any)=>{
                    if(item.menuId==data.menuId){
                        item.fun.forEach((e:any,i:any)=>{
                            
                            if(data.name){
                                console.info(e.name);
                                console.info(data.name);
                                if(e.name.indexOf(data.name)>-1){
                                    funArr.push(e);
                                }
                            }else{
                                funArr.push(e);
                            }
                            
                        })
                    }
                })
            }
        })
        return {
            code: 200,
            data: funArr,
            msg: 'success'
        }
    },

    /**
   * 用户获取列表
   */
     getUserList: (res: any) => {
        let data = JSON.parse(res.body);
        return {
            code: 200,
            data: userList,
            msg: 'success'
        }
    },
    /**
   * 聊天获取列表
   */
    getChatList: (res: any) => {
        let data = JSON.parse(res.body);
        console.info(data);
        // 聊天信息
        const chatList: any = [];
        for (let i = 0; i < data.size; i++) {
            let name=Random.cname();
            let type = Random.natural(1,3);
            let str='text';
            let conent='';
            let poster='';
            if(type==1){
                str='text';
                 conent=Random.csentence();
            }else if(type==2){
                str='img';
                conent=Random.image('400x600',Random.color(), '#000000',name);
            }else if(type==3){
                str='video';
                conent='https://file-cdn.flag.vip/5490e0e3a26db7beedd2bea327afc60b1663211216932/w/1920/h/1080/d/0.mp4';
                poster="https://file-cdn.flag.vip/5490e0e3a26db7beedd2bea327afc60b1663211216932/w/1920/h/1080/d/0.png";
            }
            chatList.push({
                id: Random.id(),
                name: name,
                createTime: Random.datetime('yyyy-MM-dd HH:mm'),
                phone: Random.integer(13000000000, 19900000000),
                avatar: Random.image('200x200',Random.color(), '#000000',name),
                conent: conent,
                poster: poster,
                self: Random.natural(0, 1),
                type: str,
            })
        }
        return {
            code: 200,
            pageCount: 2,
            page: data.page,
            data: chatList,
            msg: 'success'
        }
    },
    /**
     * 聊天信息推送
     */
    getSend: (res: any)=>{
        let data = JSON.parse(res.body);
        console.info(data);
        // 聊天信息
        const chat: any = {
            id: Random.id(),
            name: Random.cname(),
            createTime: Random.datetime('yyyy-MM-dd HH:mm'),
            phone: Random.integer(13000000000, 19900000000),
            avatar: Random.image('100x100'),
            conent: data.inputText,
            self: 1,
            type: data.type,
        };
        return {
            code: 200,
            data: chat,
            msg: 'success'
        }
    }
}
  