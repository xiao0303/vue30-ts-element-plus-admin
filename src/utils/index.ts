/**
 * 菜单返回 选中的所有父节点，用于显示路径
 * @param {Array} arr1
 * @param {number} id
 * @returns {Array}
 */
function familyTree(arr1:any, id:any) {
    const temp: Array<any> = [];
    function forFn (arr:any, id:any) {
        for (let i = 0; i < arr.length; i++) {
            const item = arr[i];
            if (item.menuId == id) {
                temp.unshift(item)
                forFn(arr1, item.parentMenuId)
                break
            } else {
                if (item.childs) {
                    forFn(item.childs, id)
                }
            }
        }
    }
    forFn(arr1, id)
    return temp
}
/**
 * 菜单返回 选中的所有父节点，用于显示路径
 * @param {Array} arr1
 * @param {number} id
 * @returns {Array}
 */
function menuTree(arr1:any) {
    const temp1: Array<any> = [];
    const temp2: Array<any> = [];
    const menuIdTemp: Array<any> = [];
    const functionId: Array<any> = [];
    // 加载菜单路由
    function forMenuFn (arr:any) {
        for (let i = 0; i < arr.length; i++) {
            const item = arr[i];
            if(menuIdTemp.includes(item.menuId)===false){
                if (isEmpty(item.childs)) {
                    menuIdTemp.unshift(item.menuId)
                    temp1.push(item)
                    forMenuFn(arr1)
                    break;
                } else {
                    if(item.childs){
                        forMenuFn(item.childs)
                    }
                }
            }
        }
    }
    forMenuFn(arr1);
    // 加载功能块页面  路由
    function forFun (arr:any) {
        for (let i = 0; i < arr.length; i++) {
            const item = arr[i];
            if(functionId.includes(item.menuId)===false){
                if (item.menu === false) {
                    functionId.unshift(item.menuId)
                    temp2.push(item)
                    forFun(arr1)
                    break;
                } else {
                    if(item.fun){
                        forFun(item.fun)
                    }
                    if(item.childs){
                        forFun(item.childs)
                    }
                }
            }
        }
    }
    forFun(arr1);
    console.info('temp1',temp1);
    console.info('temp2',temp2);
    return [...temp1,...temp2]
}
// 获取随机数
function guid(){
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0,
        v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
function isEmpty(obj: any){
    if (obj == "undefined" || obj == null || obj == "" || obj == " " || obj == undefined || JSON.stringify(obj) == "{}") {
      return true;
    } else {
      return false;
    }
}
export {
    isEmpty,
    guid,
    menuTree,
    familyTree
};
