import { createI18n } from 'vue-i18n';
import messages from './index';

const i18n = createI18n({
  messages,
  locale: localStorage.getItem('lang') || 'zh', // set locale
  fallbackLocale: 'zh', // set fallback locale
  globalInjection: true, // set global, $t
  legacy: false, // you must set `false`, to use Composition API
});

// 设置当前语言， lang - 语言code，如index.js中定义的 en/zhCN
export function setGlobalLocale(lang:any) {
  lang = lang || 'zh';
  localStorage.setItem('lang', lang);
  i18n.global.locale.value = lang;
}

export default i18n;