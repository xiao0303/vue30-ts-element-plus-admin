module.exports = {
    devServer: {
        // host: 'localhost',
        port: process.env.VUE_APP_SERVICE_PORT,// 端口号
        open: true, //配置自动启动浏览器
        hotOnly: true,// 热更新
    },
    //输出文件目录
    outputDir: 'dist',
    // eslint-loader 是否在保存的时候检查
    lintOnSave: false,
    //放置生成的静态资源 (js、css、img、fonts) 的 (相对于 outputDir 的) 目录。
    assetsDir: 'static'
}