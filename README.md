# vue3.0+ts+element+admin

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
![输入图片说明](README_files/Snipaste_2022-08-16_11-47-16.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-47-36.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-48-11.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-48-41.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-49-10.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-49-19.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-50-47.png)
![输入图片说明](README_files/Snipaste_2022-08-16_11-51-00.png)